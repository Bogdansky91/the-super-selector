import { bootstrap } from 'aurelia-bootstrapper';
import { StageComponent } from 'aurelia-testing';
import { PLATFORM } from 'aurelia-pal';
import { fail } from 'assert';
import Util from "./../../src/util";

describe('Stage File Viewer Component', () => {
    let component;
    let fileViewerSelector = "[data-automation-id='fileViewer']";
    const amountOfGeneratedFiles = 9;

    beforeEach(() => {
        component = StageComponent
            .withResources(PLATFORM.moduleName('components/app/file-viewer/file-viewer'))
            .inView('<file-viewer data.bind="testData"></file-viewer>')
            .boundTo({
                testData: Util.generateRandomFiles(amountOfGeneratedFiles)
            });
    });

    it('should render some data', done => {
        component.create(bootstrap).then(() => {
            let fileViewerRows = document.querySelectorAll(`${fileViewerSelector} tbody tr`);
            expect(fileViewerRows.length).toBe(amountOfGeneratedFiles);
            done();
        }).catch(e => {
            fail(e);
            done();
        });
    });

    afterEach(() => {
        component.dispose();
    });
});
