import { bootstrap } from 'aurelia-bootstrapper';
import { StageComponent } from 'aurelia-testing';
import { PLATFORM } from 'aurelia-pal';
import { fail } from 'assert';
import { FileStatus } from 'models/file';

function setupBaseComponent(status: FileStatus) {
    return StageComponent
        .withResources(PLATFORM.moduleName('components/app/file-status/file-status'))
        .inView('<file-status status.bind="status"></file-status>')
        .boundTo({
            status: status
        });
}

describe('File Status Component', () => {
    const statusTextSelector = "[data-automation-id='fileStatus__text']";
    const statusBlinkSelector = "[data-automation-id='fileStatus__blinker']";

    describe('with the available status', () => {
        let component = setupBaseComponent("available");

        it('should render Available', done => {
            component.create(bootstrap).then(() => {
                let statusText = document.querySelector(statusTextSelector);
                let statusBlink = document.querySelector(statusBlinkSelector);

                expect(statusText.textContent.trim()).toBe("Available");
                expect(statusBlink.className).toContain("success");
                done();
            }).catch(e => {
                fail(e);
                done();
            });
        });

        afterEach(() => {
            component.dispose();
        });
    });

    describe('with the scheduled status', () => {
        let component = setupBaseComponent("scheduled");

        it('should render Scheduled', done => {
            component.create(bootstrap).then(() => {
                let statusText = document.querySelector(statusTextSelector);
                let statusBlink = document.querySelector(statusBlinkSelector);

                expect(statusText.textContent.trim()).toBe("Scheduled");
                expect(statusBlink.className).toContain("warn");
                done();
            }).catch(e => {
                fail(e);
                done();
            });
        });

        afterEach(() => {
            component.dispose();
        });
    });

    describe('with some unknown status', () => {
        let component = setupBaseComponent("very broken" as any);

        it('should render Unknown', done => {
            component.create(bootstrap).then(() => {
                let statusText = document.querySelector(statusTextSelector);
                let statusBlink = document.querySelector(statusBlinkSelector);

                expect(statusText.textContent.trim()).toBe("Unknown");
                expect(statusBlink.className).toContain("unknown");
                done();
            }).catch(e => {
                fail(e);
                done();
            });
        });

        afterEach(() => {
            component.dispose();
        });
    });

    describe('with an undefined status', () => {
        let component = setupBaseComponent(undefined);

        it('should render Unknown', done => {
            component.create(bootstrap).then(() => {
                let statusText = document.querySelector(statusTextSelector);
                let statusBlink = document.querySelector(statusBlinkSelector);

                expect(statusText.textContent.trim()).toBe("Unknown");
                expect(statusBlink.className).toContain("unknown");
                done();
            }).catch(e => {
                fail(e);
                done();
            });
        });

        afterEach(() => {
            component.dispose();
        });
    });
});

