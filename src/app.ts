import { inject } from 'aurelia-framework';
import { ToolbarCustomAction } from 'components/app/file-viewer/models';
import { File } from 'models/file';
import FileService from 'services/file-service';
import Util from './util';

@inject(FileService)
export class App {
  files: Array<File> = [];
  customActions: Array<ToolbarCustomAction> = [{
    title: "Generate Random Data",
    icon: "refresh",
    handler: this.generateRandomFiles.bind(this),
    disabled: true
  }];

  constructor(private readonly fileService: FileService) {

  }

  bind() {
    this.fileService
      .getAll()
      .then(files => {
        this.files = files.map(f => new File(Util.uuid(), f.name, f.device, f.path, f.status));
        this.customActions[0].disabled = false;
      });
  }

  generateRandomFiles() {
    this.files = Util.generateRandomFiles();
  }
}
