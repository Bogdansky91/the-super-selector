import { faker } from '@faker-js/faker';
import { File, FileStatus } from 'models/file';

export default class Util {
    public static uuid() {
        let id = '', i, random;
        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                id += '-';
            }
            id += (i === 12 ? 4 : i === 16 ? random & 3 | 8 : random).toString(16);
        }
        return id;
    }

    public static generateRandomFiles(amount?: number) {
        if (!amount) {
            amount = faker.datatype.number({ min: 10, max: 30 });
        }

        const files: Array<File> = [];

        for (let index = 0; index < amount; index++) {
            const id = this.uuid();
            const name = faker.system.commonFileName();
            const device = faker.name.firstName();
            const path = faker.system.filePath();

            let status: FileStatus;
            const bingo = faker.datatype.number({ min: 0, max: 10 });
            if (bingo > 2) {
                status = faker.datatype.boolean() ? "available" : "scheduled";
            } else if (bingo == 2) {
                status = faker.address.cityName() as any;
            }

            files.push(new File(id, name, device, path, status));
        }

        return files;
    }
}