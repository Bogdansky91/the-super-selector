export type FileStatus = "available" | "scheduled";

export interface IFile {
    id: string;
    name: string;
    device: string;
    path: string;
    status?: FileStatus;
}

export class File implements IFile {
    constructor(
        public readonly id: string,
        public name: string,
        public device: string,
        public path: string,
        public status?: FileStatus
    ) {

    }
}