import { File } from 'models/file';

export interface SelectableFile extends File {
    $isSelected?: boolean;
}

export interface ToolbarCustomAction {
    title: string;
    icon: string;
    disabled: boolean;
    handler: () => void;
}

export class DownloadableFileInfo {
    constructor(public readonly path: string, public readonly device: string) {
    }
}