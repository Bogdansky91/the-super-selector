import { customElement, bindable, observable, computedFrom } from 'aurelia-framework';
import { TriCheckboxState } from 'components/core/tri-state-checkbox/models';
import { DownloadableFileInfo, SelectableFile, ToolbarCustomAction } from './models';

@customElement("file-viewer")
export class FileViewerCustomElement {
    @bindable data: Array<SelectableFile> = [];
    @bindable customActions: Array<ToolbarCustomAction> = [];
    @observable filesSelectionCheckboxState: TriCheckboxState = "unchecked";
    selectedFileIds: Array<string> = [];

    @computedFrom("selectedFileIds.length")
    get canDownload(): boolean {
        if (this.selectedFileIds.length === 0) {
            return false;
        }

        const downloadable = this.getDownloadableFileInfos();

        for (const selectedFileId of this.selectedFileIds) {
            if (!downloadable[selectedFileId]) {
                return false;
            }
        }

        return true;
    }

    dataChanged() {
        this.selectedFileIds.splice(0, this.selectedFileIds.length);
        this.filesSelectionCheckboxState = "unchecked";
    }

    checkboxChangeHandler(file: SelectableFile, isChecked: boolean) {
        this.handleRowSelection(file, isChecked);
    }

    rowClickHandler(file: SelectableFile, event: MouseEvent) {
        if (event && event.target) {
            const evtTarget = event.target as HTMLElement;

            if (typeof evtTarget.getAttribute === "function" && evtTarget.getAttribute("data-selection-chk")) {
                return true;
            }
        }

        const isRowSelected = !!file.$isSelected;
        this.handleRowSelection(file, !isRowSelected);
    }

    handleRowSelection(file: SelectableFile, isChecked: boolean) {
        const index = this.selectedFileIds.findIndex(sfid => sfid === file.id);

        if (isChecked) {
            index === -1 && this.selectedFileIds.push(file.id);
        } else {
            index > -1 && this.selectedFileIds.splice(index, 1);
        }

        file.$isSelected = isChecked;
        this.determineGlobalSelectionState();
    }

    filesSelectionCheckboxStateChanged(newState: TriCheckboxState) {
        if (newState === "checked") {
            this.data.forEach(file => this.handleRowSelection(file, true));
        } else if (newState === "unchecked") {
            this.data.forEach(file => this.handleRowSelection(file, false));
        }
    }

    handleDownload() {
        const downloadable = this.getDownloadableFileInfos();
        const downloadableFileInfos = this.selectedFileIds
            .filter(fid => !!downloadable[fid])
            .map(fid => {
                const downloadableFileInfo = downloadable[fid];

                return `${downloadableFileInfo.device} : ${downloadableFileInfo.path}`;
            });

        const message = ["Downloading: ",
            ...downloadableFileInfos]
            .join("\r\n");

        alert(message);
    }

    private determineGlobalSelectionState() {
        if (!this.selectedFileIds.length) {
            this.filesSelectionCheckboxState = "unchecked";
        } else if (this.selectedFileIds.length === this.data.length) {
            this.filesSelectionCheckboxState = "checked";
        } else {
            this.filesSelectionCheckboxState = "indeterminate";
        }
    }

    private getDownloadableFileInfos() {
        return this.data.reduce((dict, file) => {
            if (file.status === "available") {
                dict[file.id] = new DownloadableFileInfo(file.path, file.device);
            }

            return dict;
        }, {} as { [id: string]: DownloadableFileInfo });
    }
}