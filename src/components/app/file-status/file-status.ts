import { customElement, bindable, observable } from 'aurelia-framework';
import { FileStatus } from 'models/file';

@customElement("file-status")
export class FileStatusCustomElement {
    @bindable status: FileStatus;
    statusText: string = "Unknown";
    statusBlinkerClass: "warn" | "success" | "unknown" = "unknown";

    statusChanged(newValue: FileStatus) {
        switch (newValue) {
            case "available":
                this.statusText = "Available";
                this.statusBlinkerClass = "success";
                break;
            case "scheduled":
                this.statusText = "Scheduled";
                this.statusBlinkerClass = "warn";
                break;
            default:
                this.statusText = "Unknown";
                this.statusBlinkerClass = "unknown";
                break;
        }
    }
}