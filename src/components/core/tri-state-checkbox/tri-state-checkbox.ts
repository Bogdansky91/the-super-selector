import { customElement, bindable, inject, TaskQueue, bindingMode } from 'aurelia-framework';
import { TriCheckboxState } from './models';

@customElement("tri-state-checkbox")
@inject(TaskQueue)
export class TriStateCheckboxCustomElement {
    @bindable({ defaultBindingMode: bindingMode.twoWay }) state: TriCheckboxState;
    value: boolean;
    element: HTMLInputElement & { indeterminate: boolean };

    constructor(private readonly taskQueue: TaskQueue) {

    }

    stateChanged(newState: TriCheckboxState) {
        let shouldBeIndeterminate = true;

        if (newState === "checked") {
            this.value = true;
            shouldBeIndeterminate = false;
        } else if (newState === "unchecked") {
            this.value = false;
            shouldBeIndeterminate = false;
        }

        this.taskQueue.queueTask(() => {
            this.element.indeterminate = shouldBeIndeterminate;
        });
    }

    onChange(checkboxValue: boolean) {
        if (this.state === "indeterminate") {
            this.state = "checked";
        } else {
            this.state = checkboxValue ? "checked" : "unchecked";
        }
    }
}