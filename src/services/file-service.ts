import jsonFileData from "../data/files.json";

export default class FileService {
    public getAll(): Promise<any[]> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(jsonFileData);
            }, 200);
        });
    }
}